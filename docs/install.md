# Installazione
La presente pagina illustra le caratteristiche che il proprio smartphone o tablet deve possedere per il corretto utilizzo dell'applicazione, e le indicazioni per ottenere la stessa nel proprio dispositivo.

## Requisiti del dispositivo

Per usufruire delle funzionalità dell'applicazione in modo completo ed efficiente, il dispositivo mobile dell’utente dev’essere basato su Android 4.2 (o versione successiva) e dovrà inoltre soddisfare i seguenti requisiti: 

* Fotocamera posteriore da almeno 5MP (risoluzioni inferiori potrebbero non garantire la riuscita delle operazioni di scansione)
* Almeno 10MiB di spazio libero sulla memoria di sistema 
* Connessione ad Internet attiva ogni qualvolta si desidera ottenere un resoconto.


## Download e Installazione dell'applicazione

Se l’utente è situato in un paese in cui il servizio **Google Play Store** viene fruito regolarmente e possiede un indirizzo Gmail associato al proprio tablet o smartphone, può effettuare il download dell’applicazione accedendo al servizio. Toccare quindi l’icona di Google Play Store (presente nella schermata delle app installate sul proprio dispositivo). Individuata l’applicazione nello store, sfiorare il riquadro che la riguarda e premere **_Installa_** nella pagina apparsa. Al termine dell’installazione, l’app comparirà nella lista delle applicazioni installate nel sistema e potrà essere usata in qualsiasi momento.
