# Home

Nella schermata principale sono presenti tre schede: **HOME, CRONOLOGIA, WIKI**; per ognuna di esse vi si accede toccando il nome della scheda desiderata. 
Dalla schermata principale si potrà inoltre modificare le **impostazioni** dell'applicazione toccando il tasto dedicato (a destra del nome della scheda WIKI), ed entrando così nel menu apposito.

Nella **HOME**, sulla parte immediatamente superiore, si trova la **barra di ricerca** per l'inserimento manuale degli ingredienti (si veda la pagina [Ricerca](search.md)). Alla destra della suddetta barra è collocato il tasto dei **file allegati**, se si preferisce utilizzare una foto o codice a barre già memorizzato nel proprio dispositivo.
Nell'area sottostante, è posto il tasto per accedere alla **scansione assistita** (si veda la pagina [Acquisizione da fotocamera](./1_camera.md)). Il resto della pagina informa l'utente su eventuali aggiornamenti o su recenti modifiche apportate alle impostazioni dell'app.

