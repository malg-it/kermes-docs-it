# HOME

Questa è un'app per dispositivi mobili che nasce dall'obiettivo di guidare l'utente nell'acquisto di prodotti (alimentari, cosmetici, ecc.) compatibili con determinate esigenze dell'utente stesso, siano esse dettate da motivi di salute, di natura etica, o altro. A partire dall'elenco degli ingredienti di uno specifico prodotto (che potrà essere acquisito in diverse modalità, come descritto successivamente in questa guida), l'applicazione genererà un resoconto informativo per quel prodotto, informando l'utente sulla presente o mancata coerenza del prodotto stesso con le sue necessità.

## L'applicazione sul dispositivo

Consultare la pagina [Installazione](install.md) per informazioni dettagliate su come ottenere l'applicazione.


## Uso dell'app

**L’applicazione è destinata esclusivamente a scopi legati alle preferenze dell’utente circa i prodotti di cui usufruire o da acquistare, e non per la cura, il sollievo, il trattamento o la prevenzione di malattie. Tutte le informazioni ottenute e/o consultate tramite quest’applicazione vengono fornite solo per agevolare l’utente e non devono essere in alcun modo considerate e interpretate come un consiglio medico. Tutte le informazioni ottenute mediante l’utilizzo di quest’applicazione potrebbero non essere adeguate, accurate, complete, affidabili.**