# Revisione manuale

Talvolta l'utente necessita di modificare l'elenco di ingredienti generato in seguito alla scansione, eliminandone o correggendone qualcuno, o aggiungendo altri elementi alla lista.

Quindi, per introdurre un nuovo ingrediente sulla pagina di elenco ingredienti, toccare **Aggiungi**; da qui, scrivere il nome dell'elemento, premendo infine **Salva** per completare l'operazione, o **Annulla** per annullarla.
Selezionando invece un ingrediente già presente, si potrà eliminarlo, toccando **Elimina**, o modificarlo/sostituirlo riscrivendone il nome, premendo infine **Salva**.

