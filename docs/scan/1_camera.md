# Acquisizione da fotocamera

L'applicazione può acquisire gli ingredienti del prodotto che l'utente desidera analizzare semplicemente utilizzando la fotocamera del proprio dispositivo. Ciò può avvenire tramite **foto agli ingredienti** stessi, o mediante **scansione del codice a barre** del prodotto. 

## Foto agli ingredienti
Dalla HOME, nell'interfaccia principale, toccare il **tasto di scansione**, quindi scattare una foto agli ingredienti del prodotto. Al termine dell'operazione, premere *__Avanti__*: verrà esibito l'elenco degli ingredienti dedotto. Toccare nuovamente *__Avanti__* se l'elenco corrisponde (cioè se è completo e corretto; in caso contrario, vedere la pagina [Revisione Manuale](2_review.md) ). La lista ingredienti è ora acquisita, e l'applicazione esporrà l'esito circa la compatibilità del prodotto in questione con i criteri preimpostati (si veda [Controllo del Risultato](3_commit.md)).


## Codice a barre

Dalla HOME, nell'interfaccia principale, toccare il **tasto di scansione**, quindi inquadrare il codice a barre e attenderne la lettura. Al termine dell'operazione, premere *__Avanti__*: verrà esibito l'elenco degli ingredienti dedotto. Toccare nuovamente *__Avanti__* se l'elenco corrisponde (cioè se è completo e corretto; in caso contrario, vedere la pagina [Revisione Manuale](2_review.md) ). La lista ingredienti è ora acquisita, e l'applicazione esporrà l'esito circa la compatibilità del prodotto in questione con i criteri preimpostati (si veda il [Controllo del Risultato](3_commit.md)).

