# Trasferimento Impostazioni

Se l’utente ha già utilizzato l'applicazione in un altro dispositivo, potrà facilmente recuperare le preferenze precedentemente specificate utilizzando le proprie credenziali, **Email** e **Password**, per accedere al proprio **account** dell’applicazione. Nella schermata di primo accesso all'app quindi, immettere Email, Password, e infine toccare *__Accedi__*. Dopodiché, l’accesso al proprio account è avvenuto, e si potrà utilizzare l'app come nel precedente dispositivo.

Nel caso di primo utilizzo dell’app invece, l’utente dovrà registrarsi fornendo le credenziali prima menzionate. Toccare *__Non hai un account?__*, quindi compilare i campi con le informazioni richieste (la propria Email, password, conferma password ecc.). Dopo aver inviato i dati, l’utente è registrato e possiede un account al quale egli può accedere ogniqualvolta lo desideri, come prima spiegato.
