# Selezione dei criteri

Per cominciare ad utilizzare l'applicazione, l'utente dovrà impostare le proprie esigenze o preferenze riguardo la scelta dei prodotti da acquistare. 

Si inizi allora con la **Configurazione Assistita** presente al primo accesso all'app, toccando *__Iniziamo!__*; quindi, nella pagina **Selezione dei Criteri**, selezionare le categorie che rispecchiano le proprie necessità ("allergico", "celiaco", "diabetico", ecc.). 
Si noti che per alcune voci si aprirà una pagina in cui l'utente potrà raffinare ulteriormente le proprie impostazioni specificando singolarmente gli ingredienti da evitare, come nel caso di intolleranze o allergie. Toccare il nome dell'ingrediente per selezionarlo. Una volta scelti tutti gli ingredienti critici per quella determinata categoria, toccare *__Avanti__* per tornare alla pagina precedente, così da poter continuare con la selezione delle categorie.
Terminata la compilazione della pagina **Selezione dei Criteri**, toccare *__Avanti__* per poter ora procedere con l'utilizzo dell'applicazione accedendo all'interfaccia principale.


