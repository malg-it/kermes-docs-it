# Introduzione

Dopo aver effettuato il download e completato l’installazione, l’utente che non abbia mai fruito dell’applicazione su un proprio dispositivo dovrà procedere con una configurazione delle impostazioni personali, riguardanti dapprima le credenziali per accedere alle funzioni dell’app, e poi le preferenze intrinseche all’utilizzo dell’app stessa (si consulti rispettivamente le pagine [Trasferimento delle impostazioni](transfer.md) e [Selezione dei criteri](criteria.md)).
Queste impostazioni invece, possono essere facilmente recuperate da coloro che abbiano già utilizzato l’applicazione in precedenza (si veda [Trasferimento delle impostazioni](transfer.md)). 
