# Documentazione di **Kermes** #

Questa repository contiene i sorgenti del manuale utente del progetto Kermes.

### Struttura ###

E' possibile modificarne il contenuto direttamente dalla sezione [Source](https://bitbucket.org/malg-it/kermes-docs-it/src).

* `README.md` è il presente testo, normalmente NON va modificato.
* `mkdocs.yml` contiene l'indice dei contenuti (ed alcuni metadati). La sintassi è piuttosto semplice, e va aggiornato qualora si volessero aggiungere o rimuovere pagine.
* `docs/` (directory) contiene i files scritti in [Markdown](https://bitbucket.org/tutorials/markdowndemo) riferiti dall'indice. Si tratta del vero e proprio contenuto del manuale.

### Compilazione ed anteprima
![Documentation Status](https://readthedocs.org/projects/kermes/badge/?version=latest)

Dopo circa una decina di secondi dall'ultima modifica effettuata, è possibile consultare in anteprima il manuale generato presso [kermes.readthedocs.org](http://kermes.readthedocs.org/it/)

#### Compilazione manuale

E' possibile forzare una compilazione manuale usando cURL (ad esempio da un terminale su GNU/Linux):

`curl -X POST https://readthedocs.org/build/kermes`